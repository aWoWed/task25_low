﻿using System;
using System.Text;

namespace Task23_Library_MVC_Framework.Extensions
{
    /// <summary>
    /// Extensions for text field
    /// </summary>
    public static class TextExtensions
    {
        /// <summary>
        /// Returns part of a string up to the current length with full words
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length"></param>
        /// <returns>String with full words of specified length</returns>
        public static string CutText(string text, int length)
        {
            var words = text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if (words[0].Length > length)
                return words[0];

            var stringBuilder = new StringBuilder();

            foreach (var word in words)
            {
                if ((stringBuilder + word).Length > length)
                    return $"{stringBuilder.ToString().TrimEnd(' ')}...";
                stringBuilder.Append(word + " ");
            }
            return $"{stringBuilder.ToString().TrimEnd(' ')}...";
        }
    }
}
