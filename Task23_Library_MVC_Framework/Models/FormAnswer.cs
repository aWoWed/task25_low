﻿namespace Task23_Library_MVC_Framework.Models
{
    public class FormAnswer : BaseModel
    {
        public string Genres { get; set; }
        public string ReadingPassion { get; set; }
    }
}
