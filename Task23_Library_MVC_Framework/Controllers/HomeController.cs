﻿using System;
using System.Web.Mvc;
using Task23_Library_MVC_Framework.BLL.Services.EntityFramework;
using Task23_Library_MVC_Framework.Models;

namespace Task23_Library_MVC_Framework.Controllers
{
    /// <summary>
    /// Articles page controller
    /// </summary>
    public class HomeController : Controller
    {
        private static readonly ArticleService ArticleService = new ArticleService();
        /// <summary>
        /// Loads Articles from Db
        /// </summary>
        /// <returns>Articles Page</returns>
        public ActionResult Index()
        {
            return View(ArticleService.Get());
        }

        /// <summary>
        /// Loads Full article text
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Full article Page</returns>
        public ActionResult GetFullArticle(Guid id)
        {
            var articleDto = ArticleService.GetByKey(id);
            var article = new Article
            {
                Id = articleDto.Id,
                Name = articleDto.Name,
                Text = articleDto.Text,
                CreationDate = articleDto.CreationDate
            };
            return View("FullArticle", article);
        }

    }
}