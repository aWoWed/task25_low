﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Task23_Library_MVC_Framework.DAL.Context;
using Task23_Library_MVC_Framework.DAL.Entities.Abstract;
using Task23_Library_MVC_Framework.DAL.Models;

namespace Task23_Library_MVC_Framework.DAL.Entities.EntityFramework
{
    public class FormAnswerRepository : IFormAnswerRepository
    {
        private readonly LibraryDbContext _appDbContext;

        public FormAnswerRepository(LibraryDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        /// <summary>
        /// Gets All FormAnswers from Db
        /// </summary>
        public IQueryable<FormAnswer> Get() => _appDbContext.FormAnswers;

        /// <summary>
        /// Gets Async All FormAnswers from Db
        /// </summary>
        public Task<IQueryable<FormAnswer>> GetAsync() => Task.FromResult(_appDbContext.FormAnswers.AsQueryable());

        /// <summary>
        /// Gets FormAnswer by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>FormAnswer with current key</returns>
        public FormAnswer GetByKey(Guid key) => _appDbContext.FormAnswers.FirstOrDefault(formAnswer => formAnswer.Id == key);

        /// <summary>
        /// Gets Async FormAnswer by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>FormAnswer with current key</returns>
        public async Task<FormAnswer> GetByKeyAsync(Guid key) =>
            await _appDbContext.FormAnswers.FirstOrDefaultAsync(formAnswer => formAnswer.Id == key);

        /// <summary>
        /// Gets FormAnswer by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>FormAnswer with current name</returns>
        public IQueryable<FormAnswer> GetByName(string name) =>
            _appDbContext.FormAnswers.Where(formAnswer => formAnswer.Name == name);

        /// <summary>
        /// Gets Async FormAnswer by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>FormAnswer with current name</returns>
        public Task<IQueryable<FormAnswer>> GetByNameAsync(string name) => Task.FromResult( _appDbContext.FormAnswers.Where(formAnswer => formAnswer.Name == name));

        /// <summary>
        /// Gets FormAnswer by contains Text
        /// </summary>
        /// <param name="text"></param>
        /// <returns>FormAnswer with contains text</returns>
        public IQueryable<FormAnswer> GetByContainsText(string text) =>
            _appDbContext.FormAnswers.Where(formAnswer => formAnswer.Text.ToLower().Contains(text.ToLower()));

        /// <summary>
        /// Gets Async FormAnswer by contains Text
        /// </summary>
        /// <param name="text"></param>
        /// <returns>FormAnswer with contains text</returns>
        public Task<IQueryable<FormAnswer>> GetByContainsTextAsync(string text) => Task.FromResult(
            _appDbContext.FormAnswers.Where(formAnswer => formAnswer.Text.ToLower().Contains(text.ToLower())));

        /// <summary>
        /// Inserts or Updates FormAnswer to Db
        /// </summary>
        /// <param name="entity"></param>
        public void InsertOrUpdate(FormAnswer entity) => _appDbContext.Entry(entity).State =
            _appDbContext.FormAnswers.FirstOrDefault(formAnswer => formAnswer.Id == entity.Id) == null ? EntityState.Added : EntityState.Modified;

        /// <summary>
        /// Deletes FormAnswer with current key
        /// </summary>
        /// <param name="key"></param>
        public void DeleteByKey(Guid key) => _appDbContext.FormAnswers.Remove(new FormAnswer {Id = key});

        /// <summary>
        /// Deletes all FormAnswers
        /// </summary>
        public void DeleteAll() => _appDbContext.FormAnswers.RemoveRange(_appDbContext.FormAnswers);
    }
}
