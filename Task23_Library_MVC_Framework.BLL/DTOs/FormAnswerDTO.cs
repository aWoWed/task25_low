﻿namespace Task23_Library_MVC_Framework.BLL.DTOs
{
    public class FormAnswerDTO : BaseDTO
    {
        public string Genres { get; set; }
        public string ReadingPassion { get; set; }
    }
}
