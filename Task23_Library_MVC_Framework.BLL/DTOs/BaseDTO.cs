﻿using System;

namespace Task23_Library_MVC_Framework.BLL.DTOs
{
    public class BaseDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public string Text { get; set; }

        protected BaseDTO()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.Now;
        }
    }
}
